<?php

namespace Centersis\MenuPermissao;

use Zion\Exception\ValidationException;
use Zion\Banco\Conexao;
use Pixel\Crud\CrudUtil;

class Acao
{

    private $idAcao = null;
    private $nome = null;
    private $iconCss = null;
    private $funcaoJS = null;
    private $posicao = null;
    private $apresentacao = null;
    private $disponivelPara = [];
    private $disponivelParaSetado = false;
    private $con;

    public function __construct($idAcao)
    {
        $this->idAcao = $idAcao;

        if (!$this->idAcao) {
            throw new ValidationException('Informe o Id da Ação Corretamente!');
        }

        if (!is_string($this->idAcao)) {
            throw new ValidationException('Id da Ação deve ser uma String');
        }

        $this->con = Conexao::conectar();
        $this->con->setLog(false);

        return $this;
    }

    public function atualizarDisponibilidade($moduloCod, $acaoModuloCod)
    {
        if ($this->disponivelParaSetado === false) {
            return;
        }

        if (!$this->disponivelPara) {

            $criterio = ['moduloCod' => $moduloCod];

            (new CrudUtil())->delete('_modulo_visivel', $criterio);
            (new CrudUtil())->update('_acao_modulo', ['acaoModuloOrganogramas'], ['acaoModuloOrganogramas' > null], ['moduloCod' => $moduloCod], [], []);
        } else {

            $campos = ['acaoModuloOrganogramas'];
            $criterio = ['acaoModuloCod' => $acaoModuloCod];

            $organogramas = implode(',', $this->disponivelPara);

            $valores = ['acaoModuloOrganogramas' => ['Texto' => $organogramas]];

            (new CrudUtil())->update('_acao_modulo', $campos, $valores, $criterio, [], []);
        }
    }

    public function getIdAcao()
    {
        return $this->idAcao;
    }

    public function setDisponivelPara(Disponivel $para)
    {
        $this->disponivelParaSetado = true;

        $organogramasDisponiveis = $this->organogramasDisponiveis();

        $organogramas = $para->para();

        foreach ($organogramas as $organograma) {
            if (key_exists($organograma, $organogramasDisponiveis)) {
                $this->disponivelPara[$organograma] = $organograma;
            }
        }

        return $this;
    }

    public function getDisponivelPara()
    {
        return $this->disponivelPara;
    }

    public function getDisponivelParaSetado()
    {
        return $this->disponivelParaSetado;
    }

    public function setNome($nome)
    {
        if (!$nome) {
            throw new ValidationException('Informe o Nome Corretamente!');
        }

        if (!is_string($nome)) {
            throw new ValidationException('Nome deve ser uma String');
        }

        $this->nome = $nome;
        return $this;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setIconCss($iconCss)
    {
        if (!$iconCss) {
            throw new ValidationException('Informe o Ícone CSS Corretamente!');
        }

        if (!is_string($iconCss)) {
            throw new ValidationException('Ícone CSS deve ser uma String');
        }

        $this->iconCss = $iconCss;
        return $this;
    }

    public function getIconCss()
    {
        return $this->iconCss;
    }

    public function setFuncaoJS($funcaoJS)
    {
        if (!$funcaoJS) {
            throw new ValidationException('Informe a Função JS Corretamente!');
        }

        if (!is_string($funcaoJS)) {
            throw new ValidationException('Nome deve ser uma String');
        }

        $this->funcaoJS = $funcaoJS;
        return $this;
    }

    public function getFuncaoJS()
    {
        return $this->funcaoJS;
    }

    public function setPosicao($posicao)
    {
        if (!$posicao) {
            throw new ValidationException('Informe a posição Corretamente!');
        }

        if (!is_numeric($posicao)) {
            throw new ValidationException('Posição do Grupo deve ser um número');
        }

        $this->posicao = $posicao;
        return $this;
    }

    public function getPosicao()
    {
        return $this->posicao;
    }

    public function setApresentcao($apresentacao)
    {
        $disponiveis = ['E' => 'E', 'R' => 'R', 'I' => 'I'];

        if (!$apresentacao) {
            throw new ValidationException('Informe Apresentação Corretamente!');
        }

        $inicioApresentacao = strtoupper(substr($apresentacao, 0, 1));

        if (!key_exists($inicioApresentacao, $disponiveis)) {
            throw new ValidationException('Apresentação informada não existe!');
        }

        $this->apresentacao = $inicioApresentacao;
        return $this;
    }

    public function getApresentacao()
    {
        return $this->apresentacao;
    }

    private function organogramasDisponiveis()
    {
        $qb = $this->con->qb();

        $qb->select('*')->from('potencia');

        $potencias = $this->con->paraArray($qb, 'organogramacod', 'organogramacod');

        $potencias[1] = 1;

        return $potencias;
    }

    public function validar()
    {

        if (is_null($this->nome)) {
            throw new ValidationException('Nome da ação deve ser informada!');
        }

        if (is_null($this->posicao)) {
            throw new ValidationException('Posição deve ser informada!');
        }

        if (is_null($this->apresentacao)) {
            throw new ValidationException('Apresentação deve ser informada!');
        }
    }

}
