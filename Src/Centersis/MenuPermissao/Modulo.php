<?php

namespace Centersis\MenuPermissao;

use Zion\Exception\ValidationException;
use Zion\Banco\Conexao;
use Pixel\Crud\CrudUtil;

class Modulo
{

    private $idModulo;
    private $idGrupo = null;
    private $idReferencia = null;
    private $nomeMenu = null;
    private $descricao = null;
    private $visivelMenu = true;
    private $posicao = null;
    private $iconCss = null;
    private $namespace = null;
    private $letrasMinusculas = false;
    private $disponivelParaModulo = [];
    private $disponivelParaModuloSetado = false;
    private $criarAcao = [];
    private $atualizarAcao = [];
    private $removerAcao = [];
    private $con;
    private $permissao;

    public function __construct($idModulo)
    {
        $this->idModulo = $idModulo;

        if (!$this->idModulo) {
            throw new ValidationException('Informe o Id do Modulo Corretamente!');
        }

        if (!is_string($this->idModulo)) {
            throw new ValidationException('Id do Modulo deve ser uma String');
        }

        $this->con = Conexao::conectar();
        $this->con->setLog(false);

        $this->permissao = new Permissao();

        return $this;
    }

    public function setPermissoesAdicionais($perifilCod, $organogramaCod)
    {
        $this->permissao->addPerfilCod($perifilCod, $organogramaCod);
        return $this;
    }

    public function criar()
    {
        $dadosDoModulo = $this->dadosDoModulo();

        if ($dadosDoModulo) {
            $moduloCod = $dadosDoModulo['modulocod'];
        } else {

            $this->validacao();
            $moduloCod = $this->criaNovoModulo();
        }

        $this->registraAcoes($moduloCod);

        $this->atualizaDisponibilidade($moduloCod);
    }

    public function atualizar()
    {
        $dadosDoModulo = $this->dadosDoModulo();

        if (!$dadosDoModulo) {
            return;
        } else {
            $moduloCod = $this->updateModulo($dadosDoModulo);
        }

        $this->registraAcoes($moduloCod);

        $this->atualizaDisponibilidade($moduloCod);
    }

    private function registraAcoes($moduloCod)
    {
        $acoesAtuais = $this->acoesDoModulo($moduloCod);

        if ($this->criarAcao) {
            foreach ($this->criarAcao as $idAcao => $objAcao) {
                if (key_exists($idAcao, $acoesAtuais)) {
                    //throw new ValidationException($idAcao . ': Ação já existe!');
                    $acaoModuloCod = $acoesAtuais[$idAcao];

                    $objAcao->atualizarDisponibilidade($moduloCod, $acaoModuloCod);

                    $this->permissao->atribuirPermissoes($acaoModuloCod, $this->disponivelParaModulo);
                } else {
                    $campos = [
                        'moduloCod',
                        'acaoModuloPermissao',
                        'acaoModuloIdPermissao',
                        'acaoModuloIcon',
                        'acaoModuloFuncaoJS',
                        'acaoModuloPosicao',
                        'acaoModuloApresentacao',
                        'acaoModuloOrganogramas'
                    ];

                    $valores = [
                        'moduloCod' => ['Inteiro' => $moduloCod],
                        'acaoModuloPermissao' => ['Texto' => $objAcao->getNome()],
                        'acaoModuloIdPermissao' => ['Texto' => $objAcao->getIdAcao()],
                        'acaoModuloIcon' => ['Texto' => $objAcao->getIconCss()],
                        'acaoModuloFuncaoJS' => ['Texto' => $objAcao->getFuncaoJS()],
                        'acaoModuloPosicao' => ['Inteiro' => $objAcao->getPosicao()],
                        'acaoModuloApresentacao' => ['Texto' => $objAcao->getApresentacao()]
                    ];

                    $acaoModuloCod = (new CrudUtil())->insert('_acao_modulo', $campos, $valores, []);

                    $objAcao->atualizarDisponibilidade($moduloCod, $acaoModuloCod);

                    $this->permissao->atribuirPermissoes($acaoModuloCod, $this->disponivelParaModulo);
                }
            }
        }

        if ($this->atualizarAcao) {
            foreach ($this->atualizarAcao as $idAcao => $objAcao) {
                if (!key_exists($idAcao, $acoesAtuais)) {
                    // throw new ValidationException($idAcao . ': Não existe e por isso não pode ser atualizada!');
                } else {

                    $campos = [];
                    $valores = [];

                    if ($objAcao->getNome()) {
                        $campos[] = 'acaoModuloPermissao';
                        $valores['acaoModuloPermissao'] = ['Texto' => $objAcao->getNome()];
                    }

                    if ($objAcao->getIconCss()) {
                        $campos[] = 'acaoModuloIcon';
                        $valores['acaoModuloIcon'] = ['Texto' => $objAcao->getIconCss()];
                    }

                    if ($objAcao->getFuncaoJS()) {
                        $campos[] = 'acaoModuloFuncaoJS';
                        $valores['acaoModuloFuncaoJS'] = ['Texto' => $objAcao->getFuncaoJS()];
                    }

                    if ($objAcao->getPosicao()) {
                        $campos[] = 'acaoModuloPosicao';
                        $valores['acaoModuloPosicao'] = ['Inteiro' => $objAcao->getPosicao()];
                    }

                    if ($objAcao->getApresentacao()) {
                        $campos[] = 'acaoModuloApresentacao';
                        $valores['acaoModuloApresentacao'] = ['Texto' => $objAcao->getApresentacao()];
                    }

                    if ($campos) {
                        (new CrudUtil())->update('_acao_modulo', $campos, $valores, ['moduloCod' => $moduloCod, 'acaoModuloIdPermissao' => $idAcao], ['acaoModuloIdPermissao' => \PDO::PARAM_STR], []);
                    }

                    $objAcao->atualizarDisponibilidade($moduloCod, $acoesAtuais[$idAcao]);

                    $disponivelParaAcao = $this->atualizarAcao[$idAcao]->getDisponivelPara();
                    $disponivelParaSetado = $this->atualizarAcao[$idAcao]->getDisponivelParaSetado();

                    $this->permissao->atualizarPermissoes($acoesAtuais[$idAcao], $disponivelParaAcao, $disponivelParaSetado);
                }
            }
        }

        if ($this->removerAcao) {
            foreach ($this->removerAcao as $idAcao => $objAcao) {
                if (key_exists($idAcao, $acoesAtuais)) {
                    
                    $this->permissao->removerPermissaoDeTodos($acoesAtuais[$idAcao]);

                    (new CrudUtil())->delete('_acao_modulo', ['moduloCod' => $moduloCod, 'acaoModuloIdPermissao' => $idAcao], ['acaoModuloIdPermissao' => \PDO::PARAM_STR]);
                }
            }
        }
    }

    private function atualizaDisponibilidade($moduloCod)
    {
        $visibilidadeAtual = $this->visibilidadeDoModulo($moduloCod);

        if ($this->disponivelParaModuloSetado === false) {
            return;
        }

        if (!$this->disponivelParaModulo) {
            if ($visibilidadeAtual) {
                (new CrudUtil())->delete('_modulo_visivel', ['moduloCod' => $moduloCod]);
            }
        } else {

            foreach ($visibilidadeAtual as $organograma) {
                if (!key_exists($organograma, $this->disponivelParaModulo)) {
                    (new CrudUtil())->delete('_modulo_visivel', ['moduloCod' => $moduloCod, 'organogramaCod' => $organograma]);
                }
            }

            $encontrados = 0;
            foreach ($this->disponivelParaModulo as $organograma) {

                if (!key_exists($organograma, $visibilidadeAtual)) {

                    $encontrados++;

                    $valores = [
                        'organogramaCod' => ['Inteiro' => $organograma],
                        'moduloCod' => ['Inteiro' => $moduloCod],
                    ];

                    (new CrudUtil())->insert('_modulo_visivel', array_keys($valores), $valores, []);
                }
            }

            if (!$encontrados) {
                $valores = [
                    'organogramaCod' => ['Inteiro' => 1],
                    'moduloCod' => ['Inteiro' => $moduloCod],
                ];

                (new CrudUtil())->insert('_modulo_visivel', array_keys($valores), $valores, []);
            }
        }
    }

    private function updateModulo($dadosDoModulo)
    {

        $campos = [];
        $valores = [];

        if (!is_null($this->idGrupo)) {

            $dadosGrupo = $this->dadosDoGrupo($this->idGrupo);

            $campos[] = 'grupoCod';
            $valores['grupoCod'] = ['Inteiro' => $dadosGrupo['grupocod']];
        }


        if (!is_null($this->idReferencia)) {
            $campos[] = 'moduloCodReferente';
            $valores['moduloCodReferente'] = ['Texto' => $this->idReferencia];
        }

        if (!is_null($this->nomeMenu)) {
            $campos[] = 'moduloNomeMenu';
            $valores['moduloNomeMenu'] = ['Texto' => $this->nomeMenu];
        }

        if (!is_null($this->descricao)) {
            $campos[] = 'moduloDesc';
            $valores['moduloDesc'] = ['Texto' => $this->descricao];
        }

        if (($this->visivelMenu ? 'S' : 'N') <> $dadosDoModulo['modulovisivelmenu']) {
            $campos[] = 'moduloVisivelMenu';
            $valores['moduloVisivelMenu'] = ['Texto' => ($this->visivelMenu ? 'S' : 'N')];
        }

        if (!is_null($this->posicao)) {
            $campos[] = 'moduloPosicao';
            $valores['moduloPosicao'] = ['Inteiro' => $this->posicao];
        }

        if (!is_null($this->iconCss)) {
            $campos[] = 'moduloClass';
            $valores['moduloClass'] = ['Texto' => $this->iconCss];
        }

        if (!is_null($this->namespace)) {
            $campos[] = 'moduloNamespace';
            $valores['moduloNamespace'] = ['Texto' => $this->namespace];
        }

        if (($this->letrasMinusculas ? 'S' : 'N') <> $dadosDoModulo['moduloletrasminusculas']) {
            $campos[] = 'moduloLetrasMinusculas';
            $valores['moduloLetrasMinusculas'] = ['Texto' => ($this->letrasMinusculas ? 'S' : 'N')];
        }

        if ($campos) {
            (new CrudUtil())->update('_modulo', $campos, $valores, ['moduloNome' => $this->idModulo], ['moduloNome' => \PDO::PARAM_STR], []);
        }

        return $dadosDoModulo['modulocod'];
    }

    public function remover()
    {
        $dadosDoModulo = $this->dadosDoModulo();

        if ($dadosDoModulo) {            
            $this->removeModulo($dadosDoModulo['modulocod']);
        }
    }

    private function removeModulo($moduloCod)
    {
        $modulosDependentes = $this->existemModulosDependentes();

        if ($modulosDependentes) {
            throw new ValidationException('O Modulo não pode ser removido pois existem módulos que dependem dele!');
        }


        $this->permissao->removerPermissaoDoModulo($moduloCod);
        (new CrudUtil())->delete('_acao_modulo', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_modulo_visivel', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_modulo_label', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_log', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_upload', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_usuario_filtro', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_usuario_grid', ['moduloCod' => $moduloCod]);
        (new CrudUtil())->delete('_usuario_paginacao', ['moduloCod' => $moduloCod]);

        $modulosRemovidos = (new CrudUtil())->delete('_modulo', ['moduloCod' => $moduloCod]);

        if (!$modulosRemovidos) {
            // throw new ValidationException('Nenhum Modulo foi removido!');
        }
    }

    private function criaNovoModulo()
    {
        $campos = [
            'grupoCod',
            'moduloCodReferente',
            'moduloNome',
            'moduloNomeMenu',
            'moduloDesc',
            'moduloVisivelMenu',
            'moduloPosicao',
            'moduloClass',
            'moduloNamespace',
            'moduloLetrasMinusculas'
        ];

        $dadosGrupo = $this->dadosDoGrupo($this->idGrupo);

        $valores = [
            'grupoCod' => ['Inteiro' => $dadosGrupo['grupocod']],
            'moduloCodReferente' => ['Texto' => $this->idReferencia],
            'moduloNome' => ['Texto' => $this->idModulo],
            'moduloNomeMenu' => ['Texto' => $this->nomeMenu],
            'moduloDesc' => ['Texto' => $this->descricao],
            'moduloVisivelMenu' => ['Texto' => ($this->visivelMenu ? 'S' : 'N')],
            'moduloPosicao' => ['Inteiro' => $this->posicao],
            'moduloClass' => ['Texto' => $this->iconCss],
            'moduloNamespace' => ['Texto' => $this->namespace],
            'moduloLetrasMinusculas' => ['Texto' => ($this->letrasMinusculas ? 'S' : 'N')],
        ];

        return (new CrudUtil())->insert('_modulo', $campos, $valores, []);
    }

    public function setDisponivelPara(Disponivel $para)
    {
        $this->disponivelParaModuloSetado = true;

        $organogramasDisponiveis = $this->organogramasDisponiveis();

        $organogramas = $para->para();

        foreach ($organogramas as $organograma) {
            if (key_exists($organograma, $organogramasDisponiveis)) {
                $this->disponivelParaModulo[$organograma] = $organograma;
            }
        }

        if(!$organogramas){
            $this->disponivelParaModulo = [];
        }

        return $this;
    }

    public function setIdReferencia($idReferencia)
    {
        if (!$idReferencia) {
            throw new ValidationException('Informe o Id de Referência Corretamente!');
        }

        if (!is_string($idReferencia)) {
            throw new ValidationException('Id de Referência deve ser uma String');
        }

        $dadosModulo = $this->dadosDoModulo($idReferencia);

        if (!$dadosModulo) {
            throw new ValidationException('Módulo de referência não encontrado!');
        }

        $this->idReferencia = $dadosModulo['modulocod'];

        return $this;
    }

    public function setIdGrupo($idGrupo)
    {
        if (!$idGrupo) {
            throw new ValidationException('Informe o Id do Grupo Corretamente!');
        }

        if (!is_string($idGrupo)) {
            throw new ValidationException('Id do Grupo deve ser uma String');
        }

        if (!$this->dadosDoGrupo($idGrupo)) {
            throw new ValidationException('Grupo de referência não encontrado!');
        }

        $this->idGrupo = $idGrupo;

        return $this;
    }

    public function setNomeMenu($nomeMenu)
    {
        if (!$nomeMenu) {
            throw new ValidationException('Informe o Nome do Menu Corretamente!');
        }

        if (!is_string($nomeMenu)) {
            throw new ValidationException('Nome do Menu deve ser uma String');
        }

        $this->nomeMenu = $nomeMenu;

        return $this;
    }

    public function setDescricao($descricao)
    {
        if (!$descricao) {
            throw new ValidationException('Informe a Descrição Corretamente!');
        }

        if (!is_string($descricao)) {
            throw new ValidationException('Descrição deve ser uma String');
        }

        $this->descricao = $descricao;
        return $this;
    }

    public function setVisivelMenu($visivelMenu)
    {
        if (!is_bool($visivelMenu)) {
            throw new ValidationException('Informe Visivel no Menu Corretamente!');
        }

        $this->visivelMenu = $visivelMenu;
        return $this;
    }

    public function setPosicao($posicao)
    {
        if (!$posicao) {
            throw new ValidationException('Informe a posição Corretamente!');
        }

        if (!is_numeric($posicao)) {
            throw new ValidationException('Posição do Modulo deve ser um número');
        }

        $this->posicao = $posicao;
        return $this;
    }

    public function setIconCss($iconCss)
    {
        if (!$iconCss) {
            throw new ValidationException('Informe o Ícone CSS Corretamente!');
        }

        if (!is_string($iconCss)) {
            throw new ValidationException('Ícone CSS deve ser uma String');
        }

        $this->iconCss = $iconCss;
        return $this;
    }

    public function setNamespace($namespace)
    {
        if (!$namespace) {
            throw new ValidationException('Informe o Namespace Corretamente!');
        }

        if (!is_string($namespace)) {
            throw new ValidationException('Namespace deve ser uma String');
        }

        $this->namespace = $namespace;
        return $this;
    }

    public function setLetrasMinusculas($letrasMinusculas)
    {
        if (!is_bool($letrasMinusculas)) {
            throw new ValidationException('Informe Letras Minusculas Corretamente!');
        }

        $this->letrasMinusculas = $letrasMinusculas;
        return $this;
    }

    public function setCriarAcao(Acao $acao)
    {
        $acao->validar();

        $this->criarAcao[$acao->getIdAcao()] = $acao;

        return $this;
    }

    public function setAtualizarAcao(Acao $acao)
    {
        $this->atualizarAcao[$acao->getIdAcao()] = $acao;

        return $this;
    }

    public function setRemoverAcao(Acao $acao)
    {
        $this->removerAcao[$acao->getIdAcao()] = $acao;

        return $this;
    }

    private function dadosDoModulo($moduloNome = null)
    {
        $nome = $moduloNome ? $moduloNome : $this->idModulo;

        $qb = $this->con->qb();

        $qb->select('*')
            ->from('_modulo')
            ->where($qb->expr()->eq('moduloNome', ':moduloNome'))
            ->setParameter(':moduloNome', $nome, \PDO::PARAM_STR);

        return $this->con->execLinha($qb);
    }

    private function dadosDoGrupo($idGrupo)
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('_grupo')
            ->where($qb->expr()->eq('grupoPacote', ':grupoPacote'))
            ->setParameter(':grupoPacote', $idGrupo, \PDO::PARAM_STR);

        return $this->con->execLinha($qb);
    }

    private function existemModulosDependentes()
    {
        $qb = $this->con->qb();

        $qb->select('a.moduloCod')
            ->from('_modulo', 'a')
            ->innerJoin('a', '_modulo', 'b', 'a.moduloCod = b.moduloCodReferente')
            ->where($qb->expr()->eq('a.moduloNome', ':moduloNome'))
            ->setParameter(':moduloNome', $this->idModulo, \PDO::PARAM_STR);

        return $this->con->execNLinhas($qb);
    }

    private function visibilidadeDoModulo($moduloCod)
    {
        $qb = $this->con->qb();

        $qb->select('organogramaCod')
            ->from('_modulo_visivel')
            ->where($qb->expr()->eq('moduloCod', ':moduloCod'))
            ->setParameter(':moduloCod', $moduloCod, \PDO::PARAM_INT);

        return $this->con->paraArray($qb, 'organogramaCod', 'organogramaCod');
    }

    private function acoesDoModulo($moduloCod)
    {
        $qb = $this->con->qb();

        $qb->select('acaoModuloCod, acaoModuloIdPermissao')
            ->from('_acao_modulo')
            ->where($qb->expr()->eq('moduloCod', ':moduloCod'))
            ->setParameter(':moduloCod', $moduloCod, \PDO::PARAM_INT);

        return $this->con->paraArray($qb, 'acaoModuloCod', 'acaoModuloIdPermissao');
    }

    private function organogramasDisponiveis()
    {
        $qb = $this->con->qb();

        $qb->select('*')->from('potencia');

        $potencias = $this->con->paraArray($qb, 'organogramacod', 'organogramacod');

        $potencias[1] = 1;

        return $potencias;
    }

    private function validacao()
    {

        if (!$this->idGrupo) {
            throw new ValidationException('Informe Id do Grupo Corretamente!');
        }

        if (!is_string($this->idGrupo)) {
            throw new ValidationException('Id do Grupo deve ser uma String');
        }

        if ($this->idReferencia and!is_string($this->idReferencia)) {
            throw new ValidationException('Id de referência se infromado deve ser uma String');
        }

        if (!$this->nomeMenu) {
            throw new ValidationException('Informe Nome no Menu Corretamente!');
        }

        if (!is_string($this->nomeMenu)) {
            throw new ValidationException('Nome no Menu deve ser uma String');
        }

        if (!$this->descricao) {
            throw new ValidationException('Informe a Descrição Corretamente!');
        }

        if (!is_string($this->descricao)) {
            throw new ValidationException('Descrição deve ser uma String');
        }

        if (!is_bool($this->visivelMenu)) {
            throw new ValidationException('Visivel no Menu se infromado deve ser um boleano');
        }

        if (!$this->posicao) {
            throw new ValidationException('Informe a posição Corretamente!');
        }

        if (!is_numeric($this->posicao)) {
            throw new ValidationException('Posição do Modulo deve ser um número');
        }

        if ($this->iconCss and!is_string($this->iconCss)) {
            throw new ValidationException('Ícone CSS se infromado deve ser uma String');
        }

        if ($this->namespace and!is_string($this->namespace)) {
            throw new ValidationException('Namespace se infromado deve ser uma String');
        }

        if (!is_bool($this->letrasMinusculas)) {
            throw new ValidationException('Letras Minusculas se infromado deve ser um boleano');
        }
    }

}
