<?php

namespace Centersis\MenuPermissao;

use Zion\Banco\Conexao;
use Pixel\Crud\CrudUtil;
use Zion\Exception\ValidationException;

class Executar
{

    private $con;

    public function __construct()
    {
        $this->con = Conexao::conectar();

        $this->registrarLog();
    }

    private function registrarLog()
    {
        $pasta = SIS_DIR_BASE . 'migration_menu';

        $jaExecutados = $this->jaExecutados();

        $tipos = ['php'];

        $arquivosOrdenados = $this->arquivosOrdenados($pasta);


        $this->con->startTransaction();

        echo 'Iniciando...' . PHP_EOL;

        $executados = 0;
        foreach ($arquivosOrdenados as $nomeArquivo => $caminhoCompleto) {

            $ext = strtolower(pathinfo($nomeArquivo, PATHINFO_EXTENSION));

            if (in_array($ext, $tipos)) {

                $this->validacao($nomeArquivo);

                $textoDataHora = substr($nomeArquivo, 0, 19);

                $partes = explode('-', $textoDataHora);

                $objDataHora = new \DateTime($partes[0]
                    . '-' . $partes[1] . '-'
                    . $partes[2] . ' '
                    . $partes[3] . ':'
                    . $partes[4] . ':'
                    . $partes[5]);
                
                $versao = $objDataHora->format('Y-m-d H:i:s');

                if (key_exists($versao, $jaExecutados)) {
                    continue;
                }

                require_once $caminhoCompleto;

                echo 'Tentando executar: ' . $nomeArquivo . '  ' . PHP_EOL;

                $this->executarArquivo($nomeArquivo);

                echo $nomeArquivo . ' executado' . PHP_EOL;

                $campos = [
                    'versao',
                    'executado'
                ];

                $valores = [
                    'versao' => ['Texto' => $versao],
                    'executado' => ['Texto' => (new \DateTime)->format('Y-m-d H:i:s')],
                ];

                (new CrudUtil())->insert('menulog', $campos, $valores, []);

                $executados++;
            }
        }

        $this->con->stopTransaction();

        if (!$executados) {
            // throw new ValidationException('Nada encontrado para ser executado!');
        }

        echo 'Executado com sucesso!' . PHP_EOL;
    }

    private function arquivosOrdenados($pasta)
    {
        $ordenados = [];

        $iterator = new \FilesystemIterator($pasta);

        foreach ($iterator as $caminhoCompleto) {

            $nomeArquivo = basename($caminhoCompleto);

            $ordenados[$nomeArquivo] = $caminhoCompleto;
        }

        ksort($ordenados);

        return $ordenados;
    }

    private function executarArquivo($arquivo)
    {
        $classe = $this->nomeDaClasse($arquivo);

        if (!class_exists($classe)) {
            throw new ValidationException('O nome do arquivo | ' . $arquivo . ' | não corresponde ao nome da classe | ' . $classe);
        }

        $classeEncontrada = new $classe();

        $classeEncontrada->executar();
    }

    private function nomeDaClasse($arquivo)
    {
        $nomeClasseSemExt = substr($arquivo, 0, -4);
        $nomeClasseSemData = substr($nomeClasseSemExt, 19);

        $partesDoNome = explode('_', $nomeClasseSemData);

        foreach ($partesDoNome as $posicao => $parte) {
            $partesDoNome[$posicao] = ucwords(strtolower($parte));
        }

        return implode('', $partesDoNome);
    }

    private function validacao($arquivo)
    {
        if (strlen($arquivo) < 20 or substr_count($arquivo, '-') < 5) {
            throw new ValidationException('O nome do arquivo não é válido use yyyy-mm-dd-hh-mm-ss_xxx');
        }
    }

    private function jaExecutados()
    {
        $qb = $this->con->qb();

        $qb->select('*')->from('menulog');

        return $this->con->paraArray($qb, 'versao', 'versao');
    }

}
