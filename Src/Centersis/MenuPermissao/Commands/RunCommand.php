<?php
namespace Centersis\MenuPermissao\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class RunCommand extends Command
{
    protected function configure()
    {
        $this->setName('run')
            ->setDescription('Executa as migrations de menu');
    }
 
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (php_sapi_name() != "cli") {
            exit('sem permissão');
        }

        define('MODULO', 'Render');
        
        require __DIR__ . '/../../../../../../../bootstrap_controller.php';

        try{
        new \Centersis\MenuPermissao\Executar();
        } catch (\Exception $e) {
            echo 'Erro ao executar: ' . PHP_EOL . $e->getMessage(). PHP_EOL;
        }
    }
}