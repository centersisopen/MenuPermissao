<?php

namespace Centersis\MenuPermissao;

use Zion\Banco\Conexao;
use Pixel\Crud\CrudUtil;

class Permissao
{

    private $permissoes;
    private $con;

    public function __construct()
    {
        //Códidos de Perfils
        $this->permissoes = [
            1 => [1],
            3 => [2],
            4 => [10],
            161 => [38],
            343 => [46],
            447 => [65],
            612 => [79],
            615 => [4],
            634 => [85],
            30000 => [88],
            50000 => [10],
            60000 => [10],
            70000 => [2],
            80000 => [2],
            90000 => [2],
            100000 => [2],
            110000 => [4],
            120000 => [3],
            130000 => [3],
            190000 => [3],
            200000 => [2],
            210000 => [8],
        ];

        $this->con = Conexao::conectar();
        $this->con->setLog(false);

        return $this;
    }

    public function addPerfilCod($perifilCod, $organogramaCod)
    {
        array_push($this->permissoes[$organogramaCod], $perifilCod);
    }

    public function atualizarPermissoes($acaoModuloCod, $disponivelParaAcao, $disponivelParaSetado)
    {
        
        if (!$disponivelParaSetado) {
            return;
        }
        
        $organogramasDisponiveis = $this->organogramasDisponiveis();
        $campos = ['acaoModuloCod', 'perfilCod'];

        if ($disponivelParaAcao) {

            foreach ($disponivelParaAcao as $organogramaCod) {
                if (key_exists($organogramaCod, $organogramasDisponiveis)) {

                    $perfils = $this->permissoes[$organogramaCod];

                    foreach ($perfils as $perfilCod) {

                        if (!$this->existePermissao($perfilCod, $acaoModuloCod)) {

                            if ($this->existePerfil($organogramaCod, $perfilCod)) {
                                $valores = [
                                    'acaoModuloCod' => ['Inteiro' => $acaoModuloCod],
                                    'perfilCod' => ['Inteiro' => $perfilCod],
                                ];

                                (new CrudUtil())->insert('_permissao', $campos, $valores, []);
                            }
                        }
                    }
                }
            }

            foreach ($organogramasDisponiveis as $organogramaCod) {
                if (!key_exists($organogramaCod, $disponivelParaAcao) and $organogramaCod <> 1) {
                    $this->removerPermissaoDoOrganograma($acaoModuloCod, $organogramaCod);
                }
            }
        } else {

            $this->atribuirPermissoes($acaoModuloCod, $disponivelParaAcao);
        }
    }

    public function atribuirPermissoes($acaoModuloCod, $disponivelPara)
    {
        $organogramasDisponiveis = $this->organogramasDisponiveis();
        $campos = ['acaoModuloCod', 'perfilCod'];

        if ($disponivelPara) {
            
            //Adiciona pemissão ao root
            $disponivelPara[1] = 1;
            
            foreach ($disponivelPara as $organogramaCod) {
                if (key_exists($organogramaCod, $organogramasDisponiveis)) {

                    $perfils = $this->permissoes[$organogramaCod];                    

                    foreach ($perfils as $perfilCod) {

                        if (!$this->existePermissao($perfilCod, $acaoModuloCod)) {

                            if ($this->existePerfil($organogramaCod, $perfilCod)) {
                                $valores = [
                                    'acaoModuloCod' => ['Inteiro' => $acaoModuloCod],
                                    'perfilCod' => ['Inteiro' => $perfilCod],
                                ];

                                (new CrudUtil())->insert('_permissao', $campos, $valores, []);
                            }
                        }
                    }
                }
            }


            foreach (array_keys($this->permissoes) as $organogramaCod) {
                if (key_exists($organogramaCod, $organogramasDisponiveis)) {
                    if (!key_exists($organogramaCod, $disponivelPara)) {
                        $this->removerPermissaoDoOrganograma($acaoModuloCod, $organogramaCod);
                    }
                }
            }
        } else {

            foreach ($this->permissoes as $organogramaCod => $perfils) {
                if (key_exists($organogramaCod, $organogramasDisponiveis)) {

                    foreach ($perfils as $perfilCod) {

                        if (!$this->existePermissao($perfilCod, $acaoModuloCod)) {

                            if ($this->existePerfil($organogramaCod, $perfilCod)) {

                                $valores = [
                                    'acaoModuloCod' => ['Inteiro' => $acaoModuloCod],
                                    'perfilCod' => ['Inteiro' => $perfilCod],
                                ];

                                (new CrudUtil())->insert('_permissao', $campos, $valores, []);
                            }
                        }
                    }
                }
            }
        }
    }

    public function removerPermissaoDoOrganograma($acaoModuloCod, $organogramaCod)
    {
        $sql = "DELETE a FROM _permissao a "
            . "INNER JOIN _perfil b ON a.perfilCod = b.perfilCod "
            . "WHERE a.acaoModuloCod = " . $acaoModuloCod . " "
            . "AND b.organogramaCod = " . $organogramaCod;

        $this->con->executar($sql);
    }
    
    public function removerPermissaoDoModulo($moduloCod)
    {
        $sql = "DELETE a FROM _permissao a "
            . "INNER JOIN _acao_modulo b ON a.acaoModuloCod = b.acaoModuloCod "
            . "WHERE b.moduloCod = " . $moduloCod . " ";

        $this->con->executar($sql);
    }

    public function removerPermissaoDeTodos($acaoModuloCod)
    {
        $sql = "DELETE  FROM _permissao WHERE acaoModuloCod = " . $acaoModuloCod;

        $this->con->executar($sql);
    }

    private function existePermissao($perfilCod, $acaoModuloCod)
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('_permissao')
            ->where($qb->expr()->eq('acaoModuloCod', ':acaoModuloCod'))
            ->andWhere($qb->expr()->eq('perfilCod', ':perfilCod'))
            ->setParameter(':acaoModuloCod', $acaoModuloCod, \PDO::PARAM_INT)
            ->setParameter(':perfilCod', $perfilCod, \PDO::PARAM_INT);

        return $this->con->execNLinhas($qb);
    }

    private function existePerfil($organogramaCod, $perfilCod)
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('_perfil')
            ->where($qb->expr()->eq('organogramaCod', ':organogramaCod'))
            ->andWhere($qb->expr()->eq('perfilCod', ':perfilCod'))
            ->setParameter(':organogramaCod', $organogramaCod, \PDO::PARAM_INT)
            ->setParameter(':perfilCod', $perfilCod, \PDO::PARAM_INT);

        return $this->con->execNLinhas($qb);
    }

    private function organogramasDisponiveis()
    {
        $qb = $this->con->qb();

        $qb->select('*')->from('potencia');

        $potencias = $this->con->paraArray($qb, 'organogramacod', 'organogramacod');

        $potencias[1] = 1;

        return $potencias;
    }

}
