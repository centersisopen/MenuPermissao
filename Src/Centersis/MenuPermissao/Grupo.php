<?php

namespace Centersis\MenuPermissao;

use Zion\Exception\ValidationException;
use Zion\Banco\Conexao;
use Pixel\Crud\CrudUtil;

class Grupo
{

    private $idGrupo;
    private $nome = null;
    private $posicao = null;
    private $iconCss = null;
    private $con;

    public function __construct($idGrupo)
    {
        $this->idGrupo = $idGrupo;

        if (!$this->idGrupo) {
            throw new ValidationException('Informe o Id do Grupo Corretamente!');
        }

        if (!is_string($this->idGrupo)) {
            throw new ValidationException('Id do Grupo deve ser uma String');
        }

        $this->con = Conexao::conectar();
        $this->con->setLog(false);

        return $this;
    }

    public function setNome($nome)
    {
        if (!$nome) {
            throw new ValidationException('Informe o Nome Corretamente!');
        }

        if (!is_string($nome)) {
            throw new ValidationException('Nome deve ser uma String');
        }

        $this->nome = $nome;
        return $this;
    }

    public function setPosicao($posicao)
    {
        if (!$posicao) {
            throw new ValidationException('Informe a posição Corretamente!');
        }

        if (!is_numeric($posicao)) {
            throw new ValidationException('Posição do Grupo deve ser um número');
        }

        $this->posicao = $posicao;
        return $this;
    }

    public function setIconCss($iconCss)
    {
        if (!$iconCss) {
            throw new ValidationException('Informe o Ícone CSS Corretamente!');
        }

        if (!is_string($iconCss)) {
            throw new ValidationException('Ícone CSS deve ser uma String');
        }

        $this->iconCss = $iconCss;
        return $this;
    }

    public function criar()
    {
        $dadosDoGrupo = $this->dadosDoGrupo();

        if ($dadosDoGrupo) {
            // throw new ValidationException('Este grupo já existe!');
        } else {
            $this->validacao();
            $this->criaNovoGrupo();
        }
    }

    public function atualizar()
    {
        $dadosDoGrupo = $this->dadosDoGrupo();

        if ($dadosDoGrupo) {
            $this->updateGrupo();
        }
    }

    private function updateGrupo()
    {
        if (!is_null($this->nome)) {
            $campos[] = 'grupoNome';
            $valores['grupoNome'] = ['Texto' => $this->nome];
        }

        if (!is_null($this->posicao)) {
            $campos[] = 'grupoPosicao';
            $valores['grupoPosicao'] = ['Inteiro' => $this->posicao];
        }

        if (!is_null($this->iconCss)) {
            $campos[] = 'grupoClass';
            $valores['grupoClass'] = ['Texto' => $this->iconCss];
        }

        (new CrudUtil())->update('_grupo', $campos, $valores, ['grupoPacote' => $this->idGrupo], ['grupoPacote' => \PDO::PARAM_STR], []);        
    }

    public function remover()
    {
        $dadosDoGrupo = $this->dadosDoGrupo();

        if ($dadosDoGrupo) {            
            $this->removeGrupo();
        }
    }

    private function removeGrupo()
    {
        $modulosDependentes = $this->existemModulosDependentes();

        if ($modulosDependentes) {
            throw new ValidationException('O Grupo não pode ser removido pois existem módulos que dependem dele!');
        }

        (new CrudUtil())->delete('_grupo', ['grupoPacote' => $this->idGrupo], ['grupoPacote' => \PDO::PARAM_STR]);        
    }

    private function criaNovoGrupo()
    {
        $campos = [
            'grupoNome',
            'grupoPacote',
            'grupoPosicao',
            'grupoClass'
        ];

        $valores = [
            'grupoNome' => ['Texto' => $this->nome],
            'grupoPacote' => ['Texto' => $this->idGrupo],
            'grupoPosicao' => ['Inteiro' => $this->posicao],
            'grupoClass' => ['Texto' => $this->iconCss],
        ];

        (new CrudUtil())->insert('_grupo', $campos, $valores, []);
    }

    private function dadosDoGrupo()
    {
        $qb = $this->con->qb();

        $qb->select('*')
            ->from('_grupo')
            ->where($qb->expr()->eq('grupoPacote', ':grupoPacote'))
            ->setParameter(':grupoPacote', $this->idGrupo, \PDO::PARAM_STR);

        return $this->con->execLinha($qb);
    }

    private function existemModulosDependentes()
    {
        $qb = $this->con->qb();

        $qb->select('a.grupoCod')
            ->from('_grupo', 'a')
            ->innerJoin('a', '_modulo', 'b', 'a.grupoCod = b.grupoCod')
            ->where($qb->expr()->eq('grupoPacote', ':grupoPacote'))
            ->setParameter(':grupoPacote', $this->idGrupo, \PDO::PARAM_STR);

        return $this->con->execNLinhas($qb);
    }

    private function validacao()
    {
        if (!$this->posicao) {
            throw new ValidationException('Informe a posição Corretamente!');
        }

        if (!is_numeric($this->posicao)) {
            throw new ValidationException('Posição do Grupo deve ser um número');
        }

        if (!$this->iconCss) {
            throw new ValidationException('Informe o Ícone CSS Corretamente!');
        }

        if (!is_string($this->iconCss)) {
            throw new ValidationException('Ícone CSS deve ser uma String');
        }
    }

}
