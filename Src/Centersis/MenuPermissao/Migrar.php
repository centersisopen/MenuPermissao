<?php

namespace Centersis\MenuPermissao;

abstract class Migrar implements IMigrar
{
    public function grupo($idGrupo)
    {
        return new Grupo($idGrupo);
    }

    public function modulo($idModulo)
    {
        return new Modulo($idModulo);
    }

    public function disponivel()
    {
        return new Disponivel();
    }

    public function acao($idAcao)
    {
        return new Acao($idAcao);
    }

}
