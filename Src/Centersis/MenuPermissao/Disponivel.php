<?php

namespace Centersis\MenuPermissao;

class Disponivel
{
    private $para = [];

    public function para()
    {
        return $this->para;
    }

    public function glemt()
    {
        $this->para[1] = 1;
        $this->para[3] = 3;
        return $this;
    }

    public function glems()
    {
        $this->para[1] = 1;
        $this->para[4] = 4;
        return $this;
    }

    public function glomam()
    {
        $this->para[1] = 1;
        $this->para[161] = 161;
        return $this;
    }

    public function goemt()
    {
        $this->para[1] = 1;
        $this->para[343] = 343;
        return $this;
    }

    public function gosc()
    {
        $this->para[1] = 1;
        $this->para[447] = 447;
        return $this;
    }

    public function gorgs()
    {
        $this->para[612] = 612;
        return $this;
    }

    public function glern()
    {
        $this->para[1] = 1;
        $this->para[615] = 615;
        return $this;
    }

    public function glmece()
    {
        $this->para[1] = 1;
        $this->para[634] = 634;
        return $this;
    }

    public function gomg()
    {
        $this->para[1] = 1;
        $this->para[30000] = 30000;
        return $this;
    }

    public function gop()
    {
        $this->para[1] = 1;
        $this->para[50000] = 50000;
        return $this;
    }

    public function gobam()
    {
        $this->para[1] = 1;
        $this->para[60000] = 60000;
        return $this;
    }

    public function gor()
    {
        $this->para[1] = 1;
        $this->para[70000] = 70000;
        return $this;
    }

    public function gog()
    {
        $this->para[1] = 1;
        $this->para[80000] = 80000;
        return $this;
    }

    public function glmpi()
    {
        $this->para[1] = 1;
        $this->para[90000] = 90000;
        return $this;
    }

    public function goipe()
    {
        $this->para[1] = 1;
        $this->para[100000] = 100000;
        return $this;
    }

    public function goms()
    {
        $this->para[1] = 1;
        $this->para[110000] = 110000;
        return $this;
    }

    public function gorj()
    {
        $this->para[1] = 1;
        $this->para[120000] = 120000;
        return $this;
    }

    public function glesp()
    {
        $this->para[1] = 1;
        $this->para[130000] = 130000;
        return $this;
    }

    public function glmerj()
    {
        $this->para[1] = 1;
        $this->para[190000] = 190000;
        return $this;
    }

    public function gope()
    {
        $this->para[1] = 1;
        $this->para[200000] = 200000;
        return $this;
    }

    public function gleg()
    {
        $this->para[1] = 1;
        $this->para[210000] = 210000;
        return $this;
    }

    public function todas()
    {
        $this->para = [];
        return $this;
    }
}
